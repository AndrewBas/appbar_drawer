import 'package:appbar_drawer/awesome_drawer.dart';
import 'package:flutter/material.dart';

class AwesomeAppbar extends StatelessWidget implements PreferredSizeWidget {
  @override
  Size get preferredSize => const Size.fromHeight(100);

  @override
  Widget build(BuildContext context) {
    return AppBar(
      actions: [
        FlatButton(
          child: Icon(Icons.menu),
          onPressed: () => Scaffold.of(context).openDrawer(),
        )
      ],
    );
  }
}
