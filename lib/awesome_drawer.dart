import 'package:appbar_drawer/screens/first_page.dart';
import 'package:appbar_drawer/screens/forth_page.dart';
import 'package:appbar_drawer/screens/second_page.dart';
import 'package:appbar_drawer/screens/third_page.dart';
import 'package:flutter/material.dart';


class AwesomeDrawer extends StatelessWidget {

  void gotoForthScreen(BuildContext ctx){
    Navigator.of(ctx).push(MaterialPageRoute(builder: (_) {
      return ForthPage();
    }
    ),
    );
  }

  void gotoThirdScreen(BuildContext ctx){
    Navigator.of(ctx).push(MaterialPageRoute(builder: (_) {
      return ThirdPage();
    }
    ),
    );
  }

  void gotoSecondScreen(BuildContext ctx){
      Navigator.of(ctx).push(MaterialPageRoute(builder: (_) {
        return SecondPage();
      }
      ),
      );
    }


  void gotoMainScreen(BuildContext ctx) {
    /*NavHelper navHelper = NavHelper(ctx: ctx);
    return navHelper.onGenerateRoute();*/

      Navigator.of(ctx).push(MaterialPageRoute(builder: (_) {
        return FirstPage();
      }
      ),
      );
    }


  @override
  Widget build(BuildContext context) {

return Drawer(
      child: ListView(
        // Important: Remove any padding from the ListView.
        padding: EdgeInsets.zero,
        children: <Widget>[
          DrawerHeader(
            child: Text('Drawer Header'),
            decoration: BoxDecoration(
              color: Colors.green,
            ),
          ),
          Container(
            height: 600,
            margin: EdgeInsets.symmetric(vertical: 20.0, horizontal: 20.0),
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(10),
                  topRight: Radius.circular(10),
                  bottomLeft: Radius.circular(10),
                  bottomRight: Radius.circular(10)
              ),
              boxShadow: [
                BoxShadow(
                  color: Colors.grey.withOpacity(0.5),
                  spreadRadius: 5,
                  blurRadius: 7,
                  offset: Offset(0, 3),
                ),
              ],
            ),
            child: FlatButton(
              onPressed: () => gotoMainScreen(context),
              child: Text('gotoMain'),
            ),
          ),

          Container(
            height: 600,
            margin: EdgeInsets.symmetric(vertical: 20.0, horizontal: 20.0),
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(10),
                  topRight: Radius.circular(10),
                  bottomLeft: Radius.circular(10),
                  bottomRight: Radius.circular(10)
              ),

              boxShadow: [
                BoxShadow(
                  color: Colors.grey.withOpacity(0.5),
                  spreadRadius: 5,
                  blurRadius: 7,
                  offset: Offset(0, 3),
                ),
              ],
            ),
            child: FlatButton(
              onPressed: () => gotoSecondScreen(context),
              child: Text('gotoSecond'),
            ),
          ),

          Container(
            height: 600,
            margin: EdgeInsets.symmetric(vertical: 20.0, horizontal: 20.0),
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(10),
                  topRight: Radius.circular(10),
                  bottomLeft: Radius.circular(10),
                  bottomRight: Radius.circular(10)
              ),
              boxShadow: [
                BoxShadow(
                  color: Colors.grey.withOpacity(0.5),
                  spreadRadius: 5,
                  blurRadius: 7,
                  offset: Offset(0, 3),
                ),
              ],
            ),
            child: FlatButton(
              onPressed: () => gotoThirdScreen(context),
              child: Text('gotoThird'),
            ),
          ),

          Container(
            height: 600,
            margin: EdgeInsets.symmetric(vertical: 20.0, horizontal: 20.0),
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(10),
                  topRight: Radius.circular(10),
                  bottomLeft: Radius.circular(10),
                  bottomRight: Radius.circular(10)
              ),
              boxShadow: [
                BoxShadow(
                  color: Colors.grey.withOpacity(0.5),
                  spreadRadius: 5,
                  blurRadius: 7,
                  offset: Offset(0, 3),
                ),
              ],
            ),
            child: FlatButton(
              onPressed: () => gotoForthScreen(context),
              child: Text('gotoForth'),
            ),
          ),
        ],
      ),
    );
  }
}