import 'package:appbar_drawer/awesome_appbar.dart';
import 'package:appbar_drawer/awesome_drawer.dart';
import 'package:flutter/material.dart';

class FirstPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AwesomeAppbar(),
      body: Container(
        child: Text('First Page'),
      ),
      drawer: AwesomeDrawer(),
    );
  }
}
